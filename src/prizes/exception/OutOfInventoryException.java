package prizes.exception;

import org.drools.FactHandle;
import org.drools.definition.rule.Rule;
import org.drools.runtime.rule.Activation;
import org.drools.runtime.rule.ConsequenceExceptionHandler;
import org.drools.runtime.rule.WorkingMemory;

import java.io.*;
import java.util.Collection;

/**
 * Created by chamanx on 1/09/14.
 */
public class OutOfInventoryException extends RuntimeException {

    public static final String THERE_IS_NO_INVENTORY_FOR_THIS_RULE_PRIZE = "There is no inventory for this rule prize";
    private static final long serialVersionUID = 510l;
    private WorkingMemory workingMemory;
    private Activation    activation;

    public OutOfInventoryException( final Throwable rootCause,
                                 final WorkingMemory workingMemory,
                                 final Activation activation ){
        super( rootCause );
        this.workingMemory = workingMemory;
        this.activation = activation;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder( THERE_IS_NO_INVENTORY_FOR_THIS_RULE_PRIZE );
        Rule rule = null;

        if( activation != null && ( rule = activation.getRule() ) != null ){
            String packageName = rule.getPackageName();
            String ruleName = rule.getName();
            sb.append( "rule \"" ).append( ruleName ).append( "\" in " ).append( packageName );
        } else {
            sb.append( "rule, name unknown" );
        }
        sb.append( ": " ).append( super.getMessage() );
        return sb.toString();
    }

    public void printFactDump(){
        printFactDump( System.err );
    }

    public void  printFactDump( PrintStream pStream ){
        Collection<FactHandle> handles = (Collection<FactHandle>) activation.getFactHandles();
        for( FactHandle handle: handles ){
            Object object = workingMemory.getObject( handle );
            if( object != null ){
                pStream.println( "   Fact " + object.getClass().getSimpleName() +
                        ": " + object.toString() );
            }
        }
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
