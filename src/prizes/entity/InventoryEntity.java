package prizes.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by chamanx on 29/08/14.
 */
@Entity
@Table(name = "inventory", schema = "", catalog = "win_prize")
public class InventoryEntity {
    private int id;
    private int quantity;
    private InventoryStateEnum state;
    private Timestamp datetime;
    private PrizeEntity prize;
    private Collection<WinEntity> wins;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "quantity", nullable = false, insertable = true, updatable = true)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false, insertable = true, updatable = true, length = 45)
    public InventoryStateEnum getState() {
        return state;
    }

    public void setState(InventoryStateEnum state) {
        this.state = state;
    }

    @Basic
    @Column(name = "datetime", nullable = true, insertable = true, updatable = true)
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InventoryEntity that = (InventoryEntity) o;

        if (id != that.id) return false;
        if (quantity != that.quantity) return false;
        if (datetime != null ? !datetime.equals(that.datetime) : that.datetime != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + quantity;
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return ""+id+" Prize: "+prize.getDescription()+" Q:"+quantity+" "+state.toString();
    }

    @ManyToOne
    @JoinColumn(name = "prize", referencedColumnName = "id", nullable = false)
    public PrizeEntity getPrize() {
        return prize;
    }

    public void setPrize(PrizeEntity prize) {
        this.prize = prize;
    }

    @OneToMany(mappedBy = "inventory")
    public Collection<WinEntity> getWins() {
        return wins;
    }

    public void setWins(Collection<WinEntity> wins) {
        this.wins = wins;
    }
}
