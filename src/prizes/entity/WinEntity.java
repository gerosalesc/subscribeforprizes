package prizes.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by chamanx on 29/08/14.
 */
@Entity
@Table(name = "win", schema = "", catalog = "win_prize")
public class WinEntity {
    private int id;
    private Timestamp datetime;
    private WinStateEnum state;
    private SubscriberEntity suscriber;
    private InventoryEntity inventory;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "datetime", nullable = false, insertable = true, updatable = true)
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = true, insertable = true, updatable = true, length = 45)
    public WinStateEnum getState() {
        return state;
    }

    public void setState(WinStateEnum state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WinEntity winEntity = (WinEntity) o;

        if (id != winEntity.id) return false;
        if (datetime != null ? !datetime.equals(winEntity.datetime) : winEntity.datetime != null) return false;
        if (state != null ? !state.equals(winEntity.state) : winEntity.state != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "suscriber", referencedColumnName = "id", nullable = false)
    public SubscriberEntity getSuscriber() {
        return suscriber;
    }

    public void setSuscriber(SubscriberEntity suscriber) {
        this.suscriber = suscriber;
    }

    @ManyToOne
    @JoinColumn(name = "inventory", referencedColumnName = "id", nullable = false)
    public InventoryEntity getInventory() {
        return inventory;
    }

    public void setInventory(InventoryEntity inventory) {
        this.inventory = inventory;
    }
}
