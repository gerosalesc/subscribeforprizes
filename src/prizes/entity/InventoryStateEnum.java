package prizes.entity;

/**
 * Created by chamanx on 29/08/14.
 */
public enum InventoryStateEnum {
    ENABLED,
    DISABLED
}
