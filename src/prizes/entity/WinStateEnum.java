package prizes.entity;

/**
 * Created by chamanx on 29/08/14.
 */
public enum WinStateEnum {
    /**
     * when 2 rules collapse then the first receive prize and the second one too but if that second one does not exist at the moment then his win waits for him to come for the day
     */
    PENDING,
    /**
     * notification sent to subscriptor,
     */
    NOTIFIED,
    /**
     * Prize already given (aut of requirements for now),
     */
    GIVEN,
    /**
     * a former pending prize win that wasn't able to be assigned to any subscriptor because that subscriptor just never exist that same day
     */
    CANCELLED,
}
