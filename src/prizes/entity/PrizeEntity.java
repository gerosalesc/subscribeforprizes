package prizes.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by chamanx on 29/08/14.
 */
@Entity
@Table(name = "prize", schema = "", catalog = "win_prize")
public class PrizeEntity {
    private int id;
    private String description;
    private Collection<InventoryEntity> inventories;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "description", nullable = false, insertable = true, updatable = true, length = 45)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrizeEntity that = (PrizeEntity) o;

        if (id != that.id) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return description ;
    }

    @OneToMany(mappedBy = "prize")
    public Collection<InventoryEntity> getInventories() {
        return inventories;
    }

    public void setInventories(Collection<InventoryEntity> inventories) {
        this.inventories = inventories;
    }
}
