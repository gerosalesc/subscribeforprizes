package prizes.facade;

import prizes.entity.SubscriberEntity;
import prizes.views.JsfUtil;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by chamanx on 29/08/14.
 */
@Stateless(name = "SubscriberEJB")
public class SubscriberBean extends AbstractFacade<SubscriberEntity> implements SubscriberLocal{
    @PersistenceContext(unitName = "WinPrizesPU")
    private javax.persistence.EntityManager entityManager;

    @EJB
    private WinningRulesBean rulesEJB;
    @EJB
    private WinLocal winEJB;

    public SubscriberBean() {
        super(SubscriberEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public SubscriberEntity createSubscriberByEmail(String email) {
        SubscriberEntity subsc = new SubscriberEntity();
        subsc.setDatetime(new Timestamp(JsfUtil.getCurrentTime().getTime()));
        subsc.setMail(email);
        getEntityManager().persist(subsc);
        //Trigger winning rules logic
        rulesEJB.execute(subsc);
        return subsc;
    }

    @Override
    public SubscriberEntity findByEmailToday(String email) {
        Date todayFirstHour = JsfUtil.getFirstToday();
        Query q = getEntityManager().createQuery(
                "select subs from SubscriberEntity subs where subs.mail=?1 and subs.datetime > ?2")
                .setParameter(1, email).setParameter(2, todayFirstHour).setMaxResults(1);
        SubscriberEntity se = null;
        try {
            se = (SubscriberEntity) q.getSingleResult();
        }catch (NoResultException nre){
            //TODO: log
        }
        return se;

    }

    @Override
    public int getTodayNumber(SubscriberEntity subscriber){
        Date todayFirstHour = JsfUtil.getFirstToday();
        TypedQuery<SubscriberEntity> q = getEntityManager().createQuery(
                "select subs from SubscriberEntity subs where subs.datetime > ?1", SubscriberEntity.class)
                .setParameter(1, todayFirstHour);
        List<SubscriberEntity> resultList = q.getResultList();
        return resultList.lastIndexOf(subscriber)+1;
    }
}
