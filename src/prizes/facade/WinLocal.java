package prizes.facade;

import prizes.entity.SubscriberEntity;
import prizes.entity.WinEntity;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by chamanx on 1/09/14.
 */
@Local
public interface WinLocal extends CrudEJBLocal<WinEntity> {
    List<WinEntity> findAllTodayPendingWins();

    WinEntity getSingleSubscriberWin(SubscriberEntity subscriber);
}
