package prizes.facade;

import prizes.entity.PrizeEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by chamanx on 29/08/14.
 */
@Stateless(name = "PrizeEJB")
public class PrizeBean extends AbstractFacade<PrizeEntity> implements PrizeLocal{
    @PersistenceContext(unitName = "WinPrizesPU")
    private EntityManager entityManager;


    public PrizeBean() {
        super(PrizeEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
