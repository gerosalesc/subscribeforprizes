package prizes.facade;

import prizes.entity.InventoryEntity;
import prizes.entity.PrizeEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 29/08/14.
 */
@Local
public interface InventoryLocal extends CrudEJBLocal<InventoryEntity> {
    InventoryEntity findEnabledPrizeInventory(PrizeEntity prize);
}
