package prizes.facade;

import prizes.entity.SubscriberEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 29/08/14.
 */
@Local
public interface SubscriberLocal extends CrudEJBLocal<SubscriberEntity> {
    SubscriberEntity createSubscriberByEmail(String email);

    SubscriberEntity findByEmailToday(String email);

    int getTodayNumber(SubscriberEntity subscriber);
}
