package prizes.facade;

import prizes.entity.InventoryEntity;
import prizes.entity.InventoryStateEnum;
import prizes.entity.PrizeEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * Created by chamanx on 29/08/14.
 */
@Stateless(name = "InventoryEJB")
public class InventoryBean extends AbstractFacade<InventoryEntity> implements InventoryLocal{
    @PersistenceContext(unitName = "WinPrizesPU")
    private EntityManager entityManager;

    public InventoryBean() {
        super(InventoryEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void create(InventoryEntity entity) {
        if (entity.getState() == null){
            entity.setState(InventoryStateEnum.ENABLED);
        }
        super.create(entity);
    }

    @Override
    public InventoryEntity findEnabledPrizeInventory(PrizeEntity prize){
        TypedQuery<InventoryEntity> query = getEntityManager().createQuery(
                "select inv from InventoryEntity inv where inv.state = ?1 and inv.prize.id = ?2 order by inv.datetime desc ",
                InventoryEntity.class)
                .setParameter(1, InventoryStateEnum.ENABLED)
                .setParameter(2, prize.getId())
                .setMaxResults(1);
        InventoryEntity enabledInventory = null;
        try {
            enabledInventory = query.getSingleResult();
        }catch (NoResultException nre){
            //TODO: log this
            nre.printStackTrace();
        }
        return enabledInventory;
    }
}
