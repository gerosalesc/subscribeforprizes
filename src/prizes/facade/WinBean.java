package prizes.facade;

import prizes.entity.InventoryEntity;
import prizes.entity.SubscriberEntity;
import prizes.entity.WinEntity;
import prizes.entity.WinStateEnum;
import prizes.views.JsfUtil;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by chamanx on 1/09/14.
 */
@Stateless(name = "WinEJB")
public class WinBean extends AbstractFacade<WinEntity> implements WinLocal {
    @PersistenceContext(unitName = "WinPrizesPU")
    private EntityManager entityManager;

    public WinBean() {
        super(WinEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public List<WinEntity> findAllTodayPendingWins(){
        Date firstToday = JsfUtil.getFirstToday();
        Query query = getEntityManager().createQuery(
                "select win from WinEntity win where win.datetime > ?1 and win.state = ?2")
                .setParameter(1, firstToday).setParameter(2, WinStateEnum.PENDING);
        return query.getResultList();
    }

    @Override
    public WinEntity getSingleSubscriberWin(SubscriberEntity subscriber){
        TypedQuery<WinEntity> query = getEntityManager().createQuery(
                "select win from WinEntity win where win.suscriber.id = ?1 order by win.datetime desc", WinEntity.class)
                .setParameter(1, subscriber.getId()).setMaxResults(1);
        WinEntity win = null;
        try {
            win = query.getSingleResult();
        }catch (NoResultException nre){
            //TODO: log this
            nre.printStackTrace();
        }
        return win;
    }
}
