package prizes.facade;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Created by chamanx on 1/09/14.
 */
@Stateless(name = "WinningRulesEJB")
public class WinningRulesBean {

    @EJB
    WinLocal winEJB;
    @EJB
    SubscriberLocal subscriberEJB;
    @EJB
    PrizeLocal prizeEJB;
    @EJB
    InventoryLocal inventoryEJB;

    protected KnowledgeBase kbase;

    public WinningRulesBean() {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(
                ResourceFactory.newClassPathResource(
                        "/prizes/drools/SubscriberWinRules.drl", getClass()), ResourceType.DRL);

        if ( kbuilder.hasErrors() ) {
            System.err.println( kbuilder.getErrors().toString() );
        }
        kbase =  KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages( kbuilder.getKnowledgePackages() );
    }

    public void execute(Object o) {
        StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
        ksession.setGlobal("winEJB", winEJB);
        ksession.setGlobal("subscriberEJB", subscriberEJB);
        ksession.setGlobal("prizeEJB", prizeEJB);
        ksession.setGlobal("inventoryEJB", inventoryEJB);
        ksession.execute(o);
    }

}
