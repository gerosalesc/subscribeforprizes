package prizes.facade;

import java.util.List;

/**
 * Created by chamanx on 25/08/14.
 */
public interface CrudEJBLocal<T> {

    public void create(T entity);

    public void edit(T entity);

    public void remove(T entity);

    public T find(Object id);

    public List findAll();

    public List findRange(int[] range);

    public int count();

    public void refresh(T entity);
}
