package prizes.views;

import prizes.entity.SubscriberEntity;
import prizes.entity.WinEntity;
import prizes.facade.SubscriberLocal;
import prizes.facade.WinLocal;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chamanx on 29/08/14.
 */
@ManagedBean(name = "subscriberBean")
@ViewScoped
public class SubscriberView {

    @EJB
    private SubscriberLocal ejb;
    @EJB
    private WinLocal winEJB;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String newSubscriptor(){
        try {
            if(ejb.findByEmailToday(email)==null){
                SubscriberEntity subscriber = ejb.createSubscriberByEmail(getEmail());
                WinEntity win = winEJB.getSingleSubscriberWin(subscriber);
                if (win != null){
                    //Notify that you won
                    JsfUtil.addSuccessMessage("You won a "+win.getInventory().getPrize().getDescription()+" !!");
                }
                JsfUtil.addSuccessMessage("You were successfully subscribed");
            }else{
                JsfUtil.addErrorMessage("You are already subscribed today");
            }
        }catch (Exception e){
            e.printStackTrace();
            JsfUtil.addErrorMessage("Can't create subscription, please try again later");
        }
        return "index";
    }
}
