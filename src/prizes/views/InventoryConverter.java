package prizes.views;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.convert.Converter;
import javax.faces.context.FacesContext;
import javax.faces.component.UIComponent;

import prizes.entity.InventoryEntity;
import prizes.facade.InventoryLocal;

@ManagedBean(name = "inventoryConverter")
@RequestScoped
public class InventoryConverter implements Converter {

    @EJB
    InventoryLocal ejb;

    public InventoryConverter() {
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uIComponent, String string) {
        if (string == null || string.trim().length() == 0) {
            return null;
        }

        final int id = Integer.parseInt(string);

        return ejb.find(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object == null) return null;

        if (object instanceof InventoryEntity) {
            InventoryEntity entity = (InventoryEntity) object;

            final String pk = String.valueOf(entity.getId());

            return pk;
        } else {
            throw new IllegalArgumentException("Incorrect object type: " + object.getClass().getName() + "; must be: InventoryEntity");
        }
    }
}
