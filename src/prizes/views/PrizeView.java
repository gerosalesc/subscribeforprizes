package prizes.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import prizes.entity.PrizeEntity;

/**
 * Created by chamanx on 29/08/14.
 */
public class PrizeView {
    final public static String SELECT_ALL_ENTITIES_SQL = "SELECT o FROM PrizeEntity AS o";

    private PrizeEntity myEntity;

    private EntityManagerFactory myEntityManagerFactory;

    private ListDataModel myList;
    private ListDataModel myReferencesEntities; // M-N and M-1 references

    public PrizeView() {
        myEntityManagerFactory = Persistence.createEntityManagerFactory("WinPrizesPU");
    }

    private EntityManagerFactory getEntityManagerFactory() {
        return myEntityManagerFactory;
    }

    public PrizeEntity getEntity() {
        return myEntity;
    }

    public void setEntity(PrizeEntity entity) {
        myEntity = entity;
    }

    // add new PrizeEntity
    public String create() {
        EntityManager entityManager = getEntityManagerFactory().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(getEntity());
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            try {
                entityManager.getTransaction().rollback();
            } catch (Exception e) {
            }
        }
        entityManager.close();

        return "prizeEntityList";
    }

    // save edited PrizeEntity
    public String save() {
        EntityManager entityManager = getEntityManagerFactory().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            myEntity = entityManager.merge(getEntity());
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            try {
                entityManager.getTransaction().rollback();
            } catch (Exception e) {
            }
        }
        entityManager.close();
        return "prizeEntityList";
    }

    // delete PrizeEntity
    public String delete() {
        EntityManager entityManager = getEntityManagerFactory().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            PrizeEntity entity = getCurrentEntity();
            entity = entityManager.merge(entity);
            entityManager.remove(entity);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            try {
                entityManager.getTransaction().rollback();
            } catch (Exception e) {
            }
        }
        entityManager.close();

        return "prizeEntityList";
    }

    public DataModel getReferencedEntities() {
        return myReferencesEntities;
    }

    public void setReferencedEntities(Collection<PrizeEntity> entities) {
        myReferencesEntities = new ListDataModel(new ArrayList<PrizeEntity>(entities));
    }

    public String startCreate() {
        myEntity = new PrizeEntity();
        return "createPrizeEntity";
    }

    public String startView() {
        setEntityFromRequestParam();
        return "viewPrizeEntity";
    }

    public String startEdit() {
        setEntityFromRequestParam();
        return "editPrizeEntity";
    }

    public PrizeEntity getCurrentEntity() {
        PrizeEntity entity = getEntityFromRequestParam();

        return entity == null ? myEntity : entity;
    }

    public PrizeEntity getEntityFromRequestParam() {
        if (myList == null) return null;

        EntityManager entityManager = getEntityManagerFactory().createEntityManager();
        PrizeEntity entity = (PrizeEntity) myList.getRowData();
        entity = entityManager.merge(entity);
        entityManager.close();

        return entity;
    }

    public void setEntityFromRequestParam() {
        myEntity = getCurrentEntity();
    }

    public PrizeEntity findEntity(int id) {
        EntityManager entityManager = getEntityManagerFactory().createEntityManager();

        PrizeEntity entity = entityManager.find(PrizeEntity.class, id);

        entityManager.close();

        return entity;
    }

    public DataModel getAllEntities() {
        myList = new ListDataModel(getEntities());

        return myList;
    }

    public SelectItem[] getAllEntitiesAsSelectedItems() {
        List<PrizeEntity> entities = getEntities();
        SelectItem selectItems[] = new SelectItem[entities.size()];
        int i = 0;
        for (PrizeEntity entity : entities) {
            selectItems[i++] = new SelectItem(entity);
        }
        return selectItems;
    }

    public List<PrizeEntity> getEntities() {
        EntityManager entityManager = getEntityManagerFactory().createEntityManager();

        List<PrizeEntity> entities = (List<PrizeEntity>) entityManager.createQuery(SELECT_ALL_ENTITIES_SQL).getResultList();

        entityManager.close();

        return entities;
    }
}
