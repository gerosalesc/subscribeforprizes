package prizes.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import prizes.entity.InventoryEntity;
import prizes.entity.PrizeEntity;
import prizes.facade.InventoryLocal;
import prizes.facade.PrizeLocal;

/**
 * Created by chamanx on 29/08/14.
 */
@ManagedBean(name = "inventoryView")
@SessionScoped
public class InventoryView {

    @EJB
    InventoryLocal inventoryLocalEJB;
    @EJB
    PrizeLocal prizeLocalEJB;

    private InventoryEntity myEntity;
    private ListDataModel myList;
    private ListDataModel myReferencesEntities; // M-N and M-1 references
    private PrizeEntity newPrize;


    public InventoryEntity getEntity() {
        return myEntity;
    }

    public void setEntity(InventoryEntity entity) {
        myEntity = entity;
    }

    public PrizeEntity getNewPrize() {
        if (newPrize == null)
            newPrize = new PrizeEntity();
        return newPrize;
    }

    public void setNewPrize(PrizeEntity newPrize) {
        this.newPrize = newPrize;
    }

    public DataModel getReferencedEntities() {
        return myReferencesEntities;
    }

    public void setReferencedEntities(Collection<InventoryEntity> entities) {
        myReferencesEntities = new ListDataModel(new ArrayList<InventoryEntity>(entities));
    }

    // add new InventoryEntity
    public String create() {
        inventoryLocalEJB.create(getEntity());
        return "ListInventory";
    }

    // save edited InventoryEntity
    public String save() {
        inventoryLocalEJB.edit(getEntity());
        return "ListInventory";
    }

    public String startCreate() {
        myEntity = new InventoryEntity();
        return "CreateInventory";
    }

    public String startEdit() {
        setEntityFromRequestParam();
        return "EditInventory";
    }

    public InventoryEntity getCurrentEntity() {
        InventoryEntity entity = getEntityFromRequestParam();

        return entity == null ? myEntity : entity;
    }

    public InventoryEntity getEntityFromRequestParam() {
        if (myList == null) return null;

        InventoryEntity entity = (InventoryEntity) myList.getRowData();
        inventoryLocalEJB.edit(entity);

        return entity;
    }

    public void setEntityFromRequestParam() {
        myEntity = getCurrentEntity();
    }

    public InventoryEntity findEntity(int id) {
        InventoryEntity entity = inventoryLocalEJB.find(id);
        return entity;
    }

    public DataModel getAllEntities() {
        myList = new ListDataModel(getEntities());

        return myList;
    }

    public SelectItem[] getAllEntitiesAsSelectedItems() {
        List<InventoryEntity> entities = getEntities();
        SelectItem selectItems[] = new SelectItem[entities.size()];
        int i = 0;
        for (InventoryEntity entity : entities) {
            selectItems[i++] = new SelectItem(entity);
        }
        return selectItems;
    }

    public List<InventoryEntity> getEntities() {
        List<InventoryEntity> entities = inventoryLocalEJB.findAll();
        return entities;
    }

    public List<PrizeEntity> getAllPrizes(){
        return prizeLocalEJB.findAll();
    }

    public void createPrize(){
        prizeLocalEJB.create(getNewPrize());
        setNewPrize(new PrizeEntity());
        JsfUtil.addSuccessMessage("Prize created");
    }

}
