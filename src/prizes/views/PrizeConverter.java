package prizes.views;

import prizes.entity.PrizeEntity;
import prizes.facade.PrizeLocal;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Created by chamanx on 29/08/14.
 */
@ManagedBean(name = "prizeConverter")
@RequestScoped
public class PrizeConverter implements Converter{

    @EJB
    PrizeLocal ejb;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null || s.trim().length() == 0) {
            return null;
        }

        final int id = Integer.parseInt(s);

        return ejb.find(id);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o == null) return null;

        if (o instanceof PrizeEntity) {
            PrizeEntity entity = (PrizeEntity) o;

            final String pk = String.valueOf(entity.getId());

            return pk;
        } else {
            throw new IllegalArgumentException("Incorrect object type: " + o.getClass().getName() + "; must be: PrizeEntity");
        }
    }
}
