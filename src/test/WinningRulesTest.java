package test;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.compiler.DroolsParserException;
import org.drools.io.ResourceFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by chamanx on 1/09/14.
 */
public class WinningRulesTest {

    @Test
    public void shouldFireHelloWorld() throws IOException, DroolsParserException {
        KnowledgeBase kbase;
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(
                ResourceFactory.newClassPathResource(
                        "/prizes/drools/SubscriberWinRules.drl", getClass()), ResourceType.DRL);

        if ( kbuilder.hasErrors() ) {
            fail(kbuilder.getErrors().toString());
        }
        kbase =  KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages( kbuilder.getKnowledgePackages() );

        /*for (int i=1;i<6;i++) {
            SubscriberEntity subscriberEntity = new SubscriberEntity();
            subscriberEntity.setDatetime(new Timestamp(JsfUtil.getCurrentTime().getTime()));
            subscriberEntity.setMail("test" + i + "@domain.co");
        }*/

        assertTrue(true);
    }
}
