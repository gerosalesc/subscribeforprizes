# Subscribe for prizes

This project was built using Java technologies and this Frameworks and tools:

- Java 7 and JEE6
- Enterprise Java Beans
- Java server faces (Mojarra)
- JAAS
- PrimeFaces 3.4 
- Drools 5.4 
- JPA (EclipseLink)
- Junit 4.11 for some testing 
- MariaDB


## Building Instructions

###Project structure
***TODO:***

## Deploying Instructions

This project was deployed and tested in Glassfish server 4.0 but there wouldn't be any 
problem with any other application server. 
You must configure your application server instance according to the next instructions:

- You can setup the database using the EER diagram in the design folder. You can open the full model
using **MySQL workbench**. It is also important to include roles and user information so you can 
login to the administration when testing. You can find a MariaDB backup file [here](http://pastebin.com/bpyWMVMn).

![EER model](http://i.imgur.com/dNSA2lJ.png)

- You must copy mysql connector jar in global Glassfish dependencies
- You must configure a JDBC connection pool and a associated JDBC resource which name 
must be **SuscribeWinPool_JDBCR**. It is important that the connection pool successfully 
connects to our database objects.

![JDBC Resource in Glassfish](http://i.imgur.com/sL9sRhk.png)

- The security setup must define a security Realm called **LoginRealm** which connects to the Mysql database
 using the JDBC resource we just created.
 
![Security Realm in Glassfish](http://i.imgur.com/CGpVCVx.png)


